var operands = []
var positive = true
$(document).ready(function () {
    $("button.number").click(function (e) {
        getTerminal().val($("#terminal").val() + $(this).val());
    });
    $("#clear").click(function (e) {
        getTerminal().val("");
        operands = [];
    });
    $("#plus").click(function (e) {
        valToStack();
        positive = true;
    });
    $("#minus").click(function (e) {
        valToStack();
        positive = false;
    });
    $("#eq").click(function (e) {
        valToStack();
        positive = true;
        getTerminal().val("operating...");
        sendMessage(operands);
    });
});

function getTerminal() {
    return $("#terminal");
}

function valToStack() {
    var num = parseInt(getVal());
    if (num == 0 || isNaN(num)) return;
    operands.push(num);
    getTerminal().val("");
}

function getVal() {
    return positive ? getTerminal().val() : -getTerminal().val();
}

function sendMessage(opers) {
    // Results in a call to the OnQuery method in binding_test.cpp
    window.cefQuery({
        request: JSON.stringify({operands : operands}),
        onSuccess: function (response) {
            getTerminal().val(response);
            operands = [];
        },
        onFailure: function (error_code, error_message) {}
    });
}